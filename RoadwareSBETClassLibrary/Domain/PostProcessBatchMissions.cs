﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoadwareSBETClassLibrary.Domain
{
  public class PostProcessBatchMissions
  {
    #region fields
    private bool aborted = false;

    private DataSet btchDs;
    private DataSet btchBadDs;

    private DataTable batDt;
    private DataTable batBadDt;
    private DataTable prjDt;
    private DataTable prjBadDt;
    private DataTable extrctDt;
    private DataTable extrctBadDt;
    private DataTable prcsDt;
    private DataTable prcsBadDt;
    private DataTable smrtBsDt;
    private DataTable smrtBsBadDt;
    private DataTable stgDt;
    private DataTable stgBadDt;

    private List<string> missnIds;

    private string btchBadPrjFlNm=string.Empty;
    private string btchPrjFullFlNm;
    private string[,] dtNms = new string[6, 3] 
    { 
      {"Batch","batDt","batBadDt"},
      {"Project","prjDt","prjBadDt"},
      {"Extract","extrctDt","extrctBadDt"},
      {"Process","prcsDt","prcsBadDt"},
      {"Smartbase","smrtBsDt","smrtBsBadDt"},
      {"Stage","stgDt","stgBadDt"}
    };
    #endregion

    #region constructors
    public PostProcessBatchMissions(string batchProjectFullFileName)
    {
      btchPrjFullFlNm = batchProjectFullFileName;
      PostProcessTheBatchMissions();
    }
    #endregion

    #region methods
    private void CopyDirectory(string frmFldrNm, string toFldrNm)
    {
      if (!Directory.Exists(toFldrNm)) { Directory.CreateDirectory(toFldrNm); }
      foreach (string frmFlNm in Directory.GetFiles(frmFldrNm))
      {
        string toFlNm = Path.Combine(toFldrNm, Path.GetFileName(frmFlNm));
        File.Copy(frmFlNm, toFlNm);
      }
      foreach (string subFldrNm in Directory.GetDirectories(frmFldrNm))
      {
        string[] fldrPrts = subFldrNm.Split('\\');
        CopyDirectory(subFldrNm, Path.Combine(toFldrNm, fldrPrts[fldrPrts.Length - 1]));
      }
    }

    private void DeleteDirectory(string fldrNm)
    {
      foreach (string flNm in Directory.GetFiles(fldrNm))
      {
        File.SetAttributes(flNm, FileAttributes.Normal);
        File.Delete(flNm);
      }
      foreach (string subFldrNm in Directory.GetDirectories(fldrNm))
      {
        DeleteDirectory(subFldrNm);
      }
      Directory.Delete(fldrNm, false);
    }

    public string GetBadProjectFileName() { return btchBadPrjFlNm; }

    public bool HasMissionAborted() { return aborted; }

    private void LoadBatchMissionData()
    {
      btchDs = new DataSet();
      btchDs.ReadXml(btchPrjFullFlNm);
      btchBadDs = btchDs.Clone();

      for (int i = 0; i < dtNms.GetLength(0); i++)
      {
        //Debug.WriteLine(string.Format("{0},{1},{2}", dtNms[i,0], dtNms[i,1], dtNms[i,2]));
        if (btchDs.Tables.Contains(dtNms[i, 0]))
        {
          FieldInfo fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 1], BindingFlags.NonPublic | BindingFlags.Instance);
          fiDt.SetValue(this, btchDs.Tables[dtNms[i, 0]]);

          fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 2], BindingFlags.NonPublic | BindingFlags.Instance);
          fiDt.SetValue(this, btchBadDs.Tables[dtNms[i, 0]]);
        }
      }
    }

    private void MoveBadMissions()
    {
      string frmFldrNm = string.Format("{0}\\{1}",
                                    Path.GetDirectoryName(btchPrjFullFlNm),
                                    Path.GetFileNameWithoutExtension(btchPrjFullFlNm));
      string toFldrNm = string.Format("{0}\\{1}_BAD",
                                    Path.GetDirectoryName(btchPrjFullFlNm),
                                    Path.GetFileNameWithoutExtension(btchPrjFullFlNm));



      if (!Directory.Exists(toFldrNm)) { Directory.CreateDirectory(toFldrNm); }

      foreach (string missnId in missnIds)
      {
        string frmMissnFldrNm = Path.Combine(frmFldrNm, missnId);
        string toMissnFldrNm = Path.Combine(toFldrNm,missnId);

        if (Directory.Exists(frmMissnFldrNm))
        {
          CopyDirectory(frmMissnFldrNm, toMissnFldrNm);
          DeleteDirectory(frmMissnFldrNm);
        }

        string frmMissnFlNm = Path.Combine(frmFldrNm, string.Format("{0}.pospac", missnId));
        string toMissnFlNm = Path.Combine(toFldrNm, string.Format("{0}.pospac", missnId));

        if (File.Exists(frmMissnFlNm))
        {
          File.Copy(frmMissnFlNm, toMissnFlNm);
          File.Delete(frmMissnFlNm);
        }

        frmMissnFlNm = Path.Combine(frmFldrNm, string.Format("{0}.pospac~", missnId));
        toMissnFlNm = Path.Combine(toFldrNm, string.Format("{0}.pospac~", missnId));

        if (File.Exists(frmMissnFlNm))
        {
          File.Copy(frmMissnFlNm, toMissnFlNm);
          File.Delete(frmMissnFlNm);
        }
      }
      string frmPrjBtchLogFlNm = string.Format(@"{0}\{1}.log",
                                         frmFldrNm,
                                         Path.GetFileNameWithoutExtension(btchPrjFullFlNm));
      string toPrjBtchLogFlNm = string.Format(@"{0}\{1}.log",
                                               toFldrNm,
                                               Path.GetFileNameWithoutExtension(btchBadPrjFlNm));

      if (File.Exists(frmPrjBtchLogFlNm)) { File.Copy(frmPrjBtchLogFlNm, toPrjBtchLogFlNm); }
    }

    private void MoveBatchToBadBatch(string prjId)
    {
      string fltr = string.Format("[Project_Id] = {0}", prjId);
      string prcsFltr = string.Empty;
      string prcsId = string.Empty;
      for (int i = 0; i < dtNms.GetLength(0); i++)
      {
        if (btchDs.Tables.Contains(dtNms[i, 0]))
        {
          FieldInfo fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 1], BindingFlags.NonPublic | BindingFlags.Instance);
          MethodInfo mi = fiDt.FieldType.GetMethod("Select", new Type[] { typeof(string) });
          DataRow[] drs = null;
          if (dtNms[i, 0] != "Smartbase" && dtNms[i, 0] != "Batch")
          {
            drs = mi.Invoke(fiDt.GetValue(this), new object[] { fltr }) as DataRow[];
          }
          else
          {
            drs = mi.Invoke(fiDt.GetValue(this), new object[] { prcsFltr }) as DataRow[];
          }
          foreach (DataRow dr in drs)
          {
            FieldInfo fiBadDt = MethodBase.GetCurrentMethod().DeclaringType.GetField(dtNms[i, 2], BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo mib = fiBadDt.FieldType.GetMethod("NewRow");
            PropertyInfo piCol = fiDt.FieldType.GetProperty("Columns");
            DataRow badDr = mib.Invoke(fiBadDt.GetValue(this), null) as DataRow;
            DataColumnCollection dcc = piCol.GetValue(fiDt.GetValue(this)) as DataColumnCollection;
            foreach (DataColumn dc in dcc)
            {
              badDr[dc.ToString()] = dr[dc.ToString()];
              if (dtNms[i, 0] == "Process" && dc.ToString() == "Process_Id")
              {
                prcsId = dr[dc.ToString()].ToString();
                prcsFltr = string.Format("[Process_Id] = {0}", prcsId);
              }
            }

            PropertyInfo prptyInfo = fiBadDt.FieldType.GetProperty("Rows");

            MethodInfo mir = prptyInfo.PropertyType.GetMethod("Add", new Type[] { typeof(DataRow) });

            DataRowCollection drc = prptyInfo.GetValue(fiBadDt.GetValue(this)) as DataRowCollection;

            mir.Invoke(prptyInfo.GetValue(fiBadDt.GetValue(this)), new object[] { badDr });
          }
        }
      }
    }

    private void PostProcessTheBatchMissions()
    {
      LoadBatchMissionData();
      SeperateBatchMissionData();
      if (!aborted)
      {
        //Commented out the last two lines of code so the APP will not produce bad batch files
        //SaveSeperatedBatchMissionData();
        //if (missnIds.Count > 0) { MoveBadMissions(); }
      }
      //RemoveBadMissions();
    }

    private void RemoveBadMissions()
    {
      //// Not used, replaced with MoveBadMissions
      //string fldrNm = string.Format("{0}\\{1}",
      //                              Path.GetDirectoryName(btchPrjFlNm),
      //                              Path.GetFileNameWithoutExtension(btchPrjFlNm));

      //foreach (string missnId in missnIds)
      //{
      //  string missnFldrNm = Path.Combine(fldrNm, missnId);
      //  //MessageBox.Show(string.Format("Mission folder name:  {0}", missnFldrNm));
      //  if (Directory.Exists(missnFldrNm)) { DeleteDirectory(missnFldrNm); }
      //  string missnFlNm = string.Format("{0}.pospac", missnFldrNm);
      //  //MessageBox.Show(string.Format("Mission batch file name:  {0}",missnFlNm));
      //  if (File.Exists(missnFlNm)) { File.Delete(missnFlNm); }
      //  missnFlNm = string.Format("{0}.pospac~", missnFldrNm);
      //  //MessageBox.Show(string.Format("Mission funny batch file name:  {0}",missnFlNm));
      //  if (File.Exists(missnFlNm)) { File.Delete(missnFlNm); }
      //}
    }

    private void SaveSeperatedBatchMissionData()
    {
      btchDs.WriteXml(btchPrjFullFlNm);

      if (prjBadDt.Rows.Count > 0)
      {
        prjBadDt.Columns.Remove("Status");

        btchBadDs.Tables["Stage"].Constraints.Remove("Project_Stage");
        btchBadDs.Relations.Remove("Project_Stage");
        btchBadDs.Tables.Remove("Stage");
        string flNmExt = Path.GetExtension(btchPrjFullFlNm);

        btchBadPrjFlNm = string.Format("{0}\\{1}_BAD{2}",
                                              Path.GetDirectoryName(btchPrjFullFlNm),
                                              Path.GetFileNameWithoutExtension(btchPrjFullFlNm),
                                              Path.GetExtension(btchPrjFullFlNm));
        btchBadDs.WriteXml(btchBadPrjFlNm);
      }
    }

    private void SeperateBatchMissionData()
    {
      DataColumnCollection clmns = prjDt.Columns;
      if (clmns.Contains("Status"))
      {
        string fltr = "[Status] = 'Aborted'";
        FieldInfo fiDt = MethodBase.GetCurrentMethod().DeclaringType.GetField("prjDt", BindingFlags.NonPublic | BindingFlags.Instance);
        MethodInfo mi = fiDt.FieldType.GetMethod("Select", new Type[] { typeof(string) });
        DataRow[] drs = mi.Invoke(fiDt.GetValue(this), new object[] { fltr }) as DataRow[];
        missnIds = new List<string>();
        foreach (DataRow dr in drs)
        {
          missnIds.Add(dr["Name"].ToString());
          MoveBatchToBadBatch(dr["Project_Id"].ToString());
          dr.Delete();
        }
      }
      else
      {
        aborted = true;
      }
    }
    #endregion
  }
}
